<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/articles', 'LandingController@article')->name('articles');
    Route::get('/users', 'LandingController@user')->name('users');
    Route::get('/employees', 'LandingController@employee')->name('employees');
    Route::get('/messages', 'LandingController@usersMessages')->name('messages');
    Route::get('/messages/{id}','MessageController@userMessages')->name('userMessages');
});

Route::resource("articulo",ArticleController::class);
Route::resource("usuario",UserController::class);
Route::resource("mensaje",MessageController::class);
Route::get('/login-google', function () {
    return Socialite::driver('google')->redirect();
});

Route::get('/google-callback', function () {
    $user = Socialite::driver('google')->user();

});
require __DIR__. '/auth.php';
