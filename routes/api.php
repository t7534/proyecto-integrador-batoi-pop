<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('article',Api\ArticleController::class);
Route::apiResource('message',Api\MessageController::class);
Route::apiResource('category',Api\CategoryController::class);
Route::apiResource('tag',Api\TagController::class);
Route::apiResource('valuation',Api\ValuationsController::class);
Route::post('login', 'Api\LoginController@login');
Route::get('article', 'Api\ArticleController@filter')->name("filter");
Route::get('userArticles/{email}','Api\UserController@mostrarArticulos');
Route::apiResource('user',Api\UserController::class);
Route::apiResource('reportArticle',Api\ReportedArticlesController::class);
Route::apiResource('reportMessage',Api\ReportedMessagesController::class);
