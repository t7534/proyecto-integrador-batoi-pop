<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h3>Articulo Denunciado</h3>
    <p>Le informamos Sr/a {{ $article->owner->name }} de que acaban de denunciar su articulo <strong>"{{ $article->name }}"</strong>,
        le recomendamos que esté atento con lo que sube. De seguir así se le sancionara con la pertinete eliminacion de cuenta.
        Atentamente, el equipo de soporte.
    </p>
</body>
</html>
