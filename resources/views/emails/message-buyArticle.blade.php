<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h3>Articulo Denunciado</h3>
    <p>Le informamos Sr/a {{ $article->owner->name }} de que acaban de comprar su articulo <strong>"{{ $article->name }}"</strong>
    </p>
</body>
</html>
