<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h3>Comentario Denunciado</h3>
    <p>Le informamos Sr/a {{ $messag->sender->name }} de que acaban de denunciar su comentario:
        <br>
        <br>
        <strong>"{{ $messag->message }}"</strong>
        <br>
        <br>
        Le recomendamos que esté atento con lo que comenta. De seguir así se le sancionara con la pertinete eliminacion de cuenta.
        Atentamente, el equipo de soporte.
    </p>
</body>
</html>
