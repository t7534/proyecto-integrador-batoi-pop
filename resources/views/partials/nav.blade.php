<!-- Responsive navbar-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container px-5">
        <a class="navbar-brand" href="/">BatoiPop</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item"><a class="nav-link" aria-current="page" href="/employees">Empleados</a></li>
                <li class="nav-item"><a class="nav-link" href="/users">Users</a></li>
                <li class="nav-item"><a class="nav-link" href="/articles">Articulos</a></li>
                <li class="nav-item"><a class="nav-link" href="/messages">Mensajes</a></li>
                @if(Auth::user())
                    <li class="nav-item"><i></i><a class="nav-link active" href="/logout">Logout {{Auth::user()->name}}</a></li>
                @else
                    <li class="nav-item"><a class="link-light active" href="/login"><i class="fs-4 bi bi-egg-fried"></i></a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
