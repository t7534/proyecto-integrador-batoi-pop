@extends('layouts.menu')
@section('content')
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            @foreach($employees as $employee)
                <!-- Section-->
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <img class="card-img-top" src="storage/img/employee.png" alt="..." />
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <h5 class="fw-bolder">{{$employee->name}}</h5>
                                    <!-- Product price-->
                                    ${{$employee->email}}<br>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{$employees->links()}}
            </div>
        </div>
    </section>
@endsection
