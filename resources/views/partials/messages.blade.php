@extends('layouts.menu')
@section('content')
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <table class="table table-striped">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th>ID</th>
                            <th>Contenido del mensaje</th>
                            <th>A quien va</th>
                            <th>Denunciado</th>
                            <th>Borrar</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)

                        <tr>
                            <td>{{ $message->id }}</td>
                            <td>{{ $message->message }}</td>
                            <td>{{ $message->reciever()->first()->name }}</td>
                            <td class="text-center">
                                @isset($message->reports()->get()->first()->message_id)
                                    <i class="fs-5 text-danger bi bi-exclamation-triangle-fill"></i>
                                @endisset
                            </td>
                            <td><div class="text-center">
                                    <form method="post" action="{{route("mensaje.destroy", $message->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-outline-danger mt-auto">DELETE</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
