<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class ArticleSeeder extends Seeder
{
    public function tagsIdRandom() {
        $arrayTagId = [];
        for ($i = 0; $i < 3; $i++) {
            $tagIdRandom = Tag::inRandomOrder()->first()->id;
            $arrayTagId[$tagIdRandom] = $tagIdRandom;
        }
        return $arrayTagId;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++) {
            Article::factory()->count(1)->create()->each(function ($article) {
                $article->tags()->attach($this->tagsIdRandom());
            });
            File::copy(public_path('../storage/app/public/img/lambo.jpg'), public_path('../storage/app/public/img/'.$i.'-article.jpg'));
        }
    }
}
