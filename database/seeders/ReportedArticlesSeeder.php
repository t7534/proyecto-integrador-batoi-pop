<?php

namespace Database\Seeders;

use App\Models\ReportedArticles;
use Illuminate\Database\Seeder;

class ReportedArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportedArticles::factory(25)->create();
    }
}
