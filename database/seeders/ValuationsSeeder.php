<?php

namespace Database\Seeders;

use App\Models\Valuations;
use Illuminate\Database\Seeder;

class ValuationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Valuations::factory(10)->create();
    }
}
