<?php

namespace Database\Seeders;

use App\Models\ReportedMessages;
use Illuminate\Database\Seeder;

class ReportedMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportedMessages::factory(46)->create();
    }
}
