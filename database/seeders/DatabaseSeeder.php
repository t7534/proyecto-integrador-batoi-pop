<?php

namespace Database\Seeders;

use App\Models\ReportedArticles;
use App\Models\ReportedMessages;
use App\Models\User;
use App\Models\Valuations;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmployeeSeeder::class);
        User::factory(5)->create();
        $this->call(CategorySeeder::class);
        $this->call(TagSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(PhotoSeeder::class);
        $this->call(ValuationsSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(ReportedMessagesSeeder::class);
        $this->call(ReportedArticlesSeeder::class);
    }
}
