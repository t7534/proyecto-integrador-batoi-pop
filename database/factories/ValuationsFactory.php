<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ValuationsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $article = Article::inRandomOrder()->first();
        $buyer_id = User::inRandomOrder()->first()->id;
        while ($buyer_id == $article->owner_id) {
            $buyer_id = User::inRandomOrder()->first()->id;
        }
        $article->buyer_id = $buyer_id;
        $article->save();

        return [
            'owner_id' => $article->owner_id,
            'buyer_id' => $buyer_id,
            'article_id' => $article->id,
            'message' => $this->faker->paragraph(2),
            'valuation' => rand(0,5)
        ];
    }
}
