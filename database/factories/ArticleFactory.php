<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(10),
            'description' => $this->faker->paragraph(2),
            'price' => $this->faker->numberBetween(0,500),
            'category_id' => Category::inRandomOrder()->first(),
            'lat' => $this->faker->latitude(),
            'lng' => $this->faker->longitude(),
            'owner_id' => User::inRandomOrder()->first()->id,
            'buyer_id' => null

        ];
    }
}
