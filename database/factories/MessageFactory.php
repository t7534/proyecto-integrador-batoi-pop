<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $article = Article::inRandomOrder()->first();

        return [
            'sender_id' => User::inRandomOrder()->first()->id,
            'reciever_id' => $article->owner_id,
            'article_id' => $article->id,
            'message' => $this->faker->paragraph(2)
        ];
    }
}
