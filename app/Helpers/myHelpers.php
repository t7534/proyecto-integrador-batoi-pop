<?php
function obtenerNombreUsuario($id)
{
    return App\Models\User::findOrFail($id)->name;
}
function obtenerNombreArticle($id)
{
    return App\Models\Article::findOrFail($id)->name;
}

function fecha($fecha)
{
    return Carbon\Carbon::parse($fecha)->locale('es')->isoFormat('MMM D, YYYY [a las] HH:mm');
}

function mediaValoracion($valoraciones) {
    $conteo = null;
    if (count($valoraciones) > 0) {
        foreach ($valoraciones as $valoracion) {
            $conteo += $valoracion->valuation;
        }
        return round($conteo / count($valoraciones));
    }
    return 0;
}

function convertirValoracion($valoraciones) {
    $valoracionesConvertidas = [];
    if (count($valoraciones) > 0) {
        foreach ($valoraciones as $valoracion) {
            array_push($valoracionesConvertidas, new \App\Http\Resources\ValuationsResource($valoracion));
        }
    }
    return $valoracionesConvertidas;
}

function getBoundaries($lat, $lng, $distance, $earthRadius = 6371)
{
    $return = array();

    // Los angulos para cada dirección
    $cardinalCoords = array('north' => '0',
        'south' => '180',
        'east' => '90',
        'west' => '270');

    $rLat = deg2rad($lat);
    $rLng = deg2rad($lng);
    $rAngDist = $distance/$earthRadius;

    foreach ($cardinalCoords as $name => $angle)
    {
        $rAngle = deg2rad($angle);
        $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
        $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));

        $return[$name] = array('lat' => (float) rad2deg($rLatB),
            'lng' => (float) rad2deg($rLonB));
    }

    return array('min_lat'  => $return['south']['lat'],
        'max_lat' => $return['north']['lat'],
        'min_lng' => $return['west']['lng'],
        'max_lng' => $return['east']['lng']);
}
