<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ValuationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'buyer_name' => obtenerNombreUsuario($this->buyer_id),
            'article_name' => obtenerNombreArticle($this->article_id),
            'valuation' => $this->valuation,
            'message' => $this->message,
            'created_at' => fecha($this->created_at)
        ];
    }
}
