<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'sender_id' => obtenerNombreUsuario($this->sender_id),
            'reciever_id' => obtenerNombreUsuario($this->reciever_id),
            'message_id' => $this->id,
            'message' => $this->message,
            'created_at' => fecha($this->created_at)
        ];
    }
}
