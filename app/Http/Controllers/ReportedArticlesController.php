<?php

namespace App\Http\Controllers;

use App\Models\ReportedArticles;
use Illuminate\Http\Request;

class ReportedArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function show(ReportedArticles $reportedArticles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportedArticles $reportedArticles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportedArticles $reportedArticles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportedArticles $reportedArticles)
    {
        //
    }
}
