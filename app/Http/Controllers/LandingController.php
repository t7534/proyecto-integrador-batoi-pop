<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Message;
use App\Models\User;

class LandingController extends Controller
{
    public function article() {
        $articles = Article::simplePaginate(8);
        $categories = Category::all();
        return view("partials.articles", compact('articles', 'categories'));
    }

    public function user() {
        $users = User::simplePaginate(8);
        return view("partials.users", compact('users'));
    }

    public function employee() {
        $employees = Employee::simplePaginate(8);
        return view("partials.employees", compact('employees'));
    }

    public function usersMessages() {
        $users = User::simplePaginate(8);
        return view("partials.usersMessages", compact('users'));
    }
}
