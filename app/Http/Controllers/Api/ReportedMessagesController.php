<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\MessageReportMessage;
use App\Models\ReportedMessages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ReportedMessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        $isReport = ReportedMessages::where(['user_id' => $user->id,'message_id' => $request->message_id])->first();
        if ($isReport) {
            return response()->json(['err' => 'Este usuario ya ha reportado este comentario'], 401);

        } else {
            $reportedMessage = new ReportedMessages();
            $reportedMessage->user_id = $user->id;
            $reportedMessage->message_id = $request->message_id;
            $reportedMessage->message = $request->message;
            $reportedMessage->save();
            Mail::to($reportedMessage->messager->sender->email)->send(new MessageReportMessage($reportedMessage->messager));
            return response()->json($reportedMessage->messager, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportedMessages  $reportedMessages
     * @return \Illuminate\Http\Response
     */
    public function show(ReportedMessages $reportedMessages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportedMessages  $reportedMessages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportedMessages $reportedMessages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportedMessages  $reportedMessages
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportedMessages $reportedMessages)
    {
        //
    }
}
