<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\MessageReportArticle;
use App\Models\Article;
use App\Models\ReportedArticles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ReportedArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        $isReport = ReportedArticles::where(['user_id' => $user->id,'article_id' => $request->article_id])->first();
        if ($isReport) {
            return response()->json(['err' => 'Este usuario ya ha reportado este articulo'], 401);

        } else {
            $reportedArticle = new ReportedArticles();
            $reportedArticle->user_id = $user->id;
            $reportedArticle->article_id = $request->article_id;
            $reportedArticle->message = $request->message;
            $reportedArticle->save();
            Mail::to($reportedArticle->article->owner->email)->send(new MessageReportArticle($reportedArticle->article));
            return response()->json($reportedArticle, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function show(ReportedArticles $reportedArticles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportedArticles $reportedArticles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportedArticles  $reportedArticles
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportedArticles $reportedArticles)
    {
        //
    }
}
