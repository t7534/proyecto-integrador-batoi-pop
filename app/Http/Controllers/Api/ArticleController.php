<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\MessageBuyArticle;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use function PHPUnit\Framework\isNull;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::get()->groupBy('category_id');
        return response()->json($articles,200);
    }

    public function filter(Request $request) {
        $email = $request->get('email');
        $id = $request->get('id')??null;
        $name = "%".$request->get('name')."%";
        $tag = $request->get('tag')??null;
        $min = $request->get('min')??null;
        $max = $request->get('max')??null;
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $km = $request->get('km');
        $box = getBoundaries($lat, $lng, $km);

        $user = User::where('email', $email)->first();

        $articles = Article::where('owner_id', '!=' ,$user->id);

        $articles = $articles->where('buyer_id', NULL);

        $articles = $articles->where("name", "like", $name);

        if ($id) $articles = $articles->where("category_id", $id);

        if ($tag) $articles = $articles->whereIn('id', function ($query){
            $query->select('article_id')->from('article_tags')->where('tag_id', request('tag'));
        });

        if (isNull($min) && isNull($max)) $articles = $articles->whereBetween('price', [$min, $max]);

        if ($lat && $lng && $km) {
            $articles = $articles->select('SELECT *, (6371 * ACOS(
                                            SIN(RADIANS(lat))
                                            * SIN(RADIANS(' . $lat . '))
                                            + COS(RADIANS(lng - ' . $lng . '))
                                            * COS(RADIANS(lat))
                                            * COS(RADIANS(' . $lat . '))
                                            )
                               ) AS distance
                     FROM articles
                     WHERE (lat BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
                     AND (lng BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
                     HAVING distance  < ' . $km . '
                     ORDER BY distance ASC ');



                                        /*selectRaw("*,
                                      ( 6371 * acos( cos( radians(" . $lat . ") ) *
                                      cos( radians(latitud) ) *
                                      cos( radians(longitud) - radians(" . $lng . ") ) +
                                      sin( radians(" . $lat . ") ) *
                                      sin( radians(latitud) ) ) )
                                      AS distance")->having("distance", "<", $km);*/
        }

        return response()->json($articles->paginate(8), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $article = new Article();
       $article->name = $request->name;
       $article->description = $request->description;
       $article->price = $request->price;
       $article->category_id = $request->category;
       $article->lat = $request->location['lat'];
       $article->lng = $request->location['lng'];
       $user = User::where('email',$request->email)->first()->id;
       $article->owner_id = $user;
       $article->buyer_id = null;
       $allTags = [];

       for ($i = 0; $i < count($request->tags); $i++) {
            $allTags[$i] = $request->tags[$i]['id'];
       }
       $article->save();
       $article->tags()->attach($allTags);
       return response()->json($article,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return response()->json($article,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $article = Article::findOrFail($request->article_id);
        $user = User::where('email',$request->email)->first();
        $article->buyer_id = $user->id;
        $article->save();
        Mail::to($article->owner->email)->send(new MessageBuyArticle($article));
        return  response()->json($article, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return response()->json($article, 204);
    }
}
