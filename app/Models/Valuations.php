<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Valuations extends Model
{
    use HasFactory;

    public function rate() {
        return $this->belongsTo(User::class);
    }

    public function rated() {
        return $this->belongsTo(User::class);
    }
}
