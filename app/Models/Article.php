<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    use HasFactory;

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function owner() {
        return $this->belongsTo(User::class);
    }

    public function buyer() {
        return $this->hasOne(User::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class,'article_tags');
    }

    public function photo() {
        return $this->hasMany(Photo::class);
    }

    public function messages() {
        return $this->hasMany(Message::class,'article_id');
    }

    public function reports() {
        return $this->hasMany(ReportedArticles::class, 'article_id');
    }
}
