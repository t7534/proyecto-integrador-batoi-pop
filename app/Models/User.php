<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function owns() {
        return $this->hasMany(Article::class,'owner_id');
    }

    public function buys() {
        return $this->hasMany(Article::class,'buyer_id');
    }

    public function rate() {
        return $this->hasMany(Valuations::class,'buyer_id');
    }

    public function rated() {
        return $this->hasMany(Valuations::class,'owner_id');
    }

    public function sends() {
        return $this->hasMany(Message::class,'sender_id');
    }

    public function recieves() {
        return $this->hasMany(Message::class,'reciever_id');
    }

    public function reportM() {
        return $this->hasMany(ReportedMessages::class,'user_id');
    }

    public function reportA() {
        return $this->hasMany(ReportedArticles::class,'user_id');
    }
}
